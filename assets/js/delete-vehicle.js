function deleteVehicle(id) {
    ajaxDeleteItem(id, indicateSuccess, indicateFailure);
}

function indicateSuccess() {
    //pop up and reload
        const message = document.getElementById("delete-msg");
        message.hidden = false;
        message.innerText = "vehicle successfully deleted";
}

function indicateFailure() {
        const message = document.getElementById("delete-msg");
        message.hidden = false;
        message.innerText = "There was an issue deleting vehicle   ";
}