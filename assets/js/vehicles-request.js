xhr.open("GET", "http://52.186.143.229/items"); // ready state 1
// let token = sessionStorage.getItem("token"); // we could set the Auth header with this value instead
// xhr.setRequestHeader("Authorization", token);

xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
        if (xhr.status == 200) {
            const items = JSON.parse(xhr.responseText);
            console.log(items);
            renderItemsInTable(items);
        } else {
            console.log("something went wrong with your request")
        }
    }
}
xhr.send(); // ready state 2,3,4 follow

function renderItemsInTable(itemsList) {
    // document.getElementById("items-div").hidden = false;

    const content = document.getElementById("items-row");
    for (let item of itemsList) {
        let newItem = document.createElement("div");
        newItem.className = "col-md-6";
        newItem.innerHTML =
            `<div class="product-item"><img src="assets/images/product-3-370x270.jpg" alt="">
             <div class="down-content">
             <h4>${item.make}  ${item.model} ${item.year}</h4>
             <h6><small><del> $ ${item.price + 1999}</del></small> $ ${item.price}</h6>
             <div class="product-action">
               <a class="action" id="delete-icon" onclick="deleteVehicle(${item.id})">Delete
               <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-trash trashIcon" viewBox="0 0 16 16">
  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
</svg></a>
 <a class="action" id="edit-icon" data-toggle="modal" data-target="#exampleModal" data-id=${item.id} data-make=${item.make} data-model=${item.model} data-year=${item.year} data-price=${item.price} data-mileage=${item.mileage}>Edit
<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-pencil-square icon" viewBox="0 0 16 16">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
</svg></a></div>
             </div></div>`;
        content.appendChild(newItem);

    }
}


