const xhr = new XMLHttpRequest();

function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken){
    // const xhr = new XMLHttpRequest(); // ready state 0
    xhr.open(method, url); // ready state 1
    if(authToken){
        xhr.setRequestHeader("Authorization", authToken);
        // xhr.setRequestHeader("Authorization", "admin-auth-token");
    }
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status>199 && xhr.status<300){
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if(body){
        xhr.send(body);
    } else {
        xhr.send(); // ready state 2,3,4 follow
    }
}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken){
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken);
}
function sendAjaxGet(url, successCallback, failureCallback, authToken){
    sendAjaxRequest("GET", url, successCallback, failureCallback, authToken);
}
function sendAjaxDelete(url, successCallback, failureCallback, authToken){
    sendAjaxRequest("DELETE", url, successCallback, failureCallback, authToken);
}
function sendAjaxPut(url, body, successCallback, failureCallback, authToken){
    sendAjaxRequest("PUT", url, body, successCallback, failureCallback, authToken);
}

function ajaxLogin(username, password, successCallback, failureCallback){
    const payload = `username=${username}&password=${password}`; // "username="+username+"&password"+password
    sendAjaxPost("http://52.186.143.229/login", payload, successCallback, failureCallback);
}

function ajaxCreateItem(item, successCallback, failureCallback){
    const itemJson = JSON.stringify(item);
    const auth = sessionStorage.getItem("token");
    sendAjaxPost("http://52.186.143.229/items", itemJson, successCallback, failureCallback, auth);
}

function ajaxDeleteItem(id, successCallback, failureCallback) {
    const auth = sessionStorage.getItem("token");
    let url = "http://52.186.143.229/items/"+id;
    sendAjaxDelete(url, successCallback, failureCallback, auth);
}

function ajaxGetById(id, successCallback, failureCallback) {
    const auth = sessionStorage.getItem("token");
    let url = "http://52.186.143.229/items/"+id;
    sendAjaxGet(url, successCallback, failureCallback, auth);
}


function ajaxUpdateItem(vehicle, successCallback, failureCallback) {
    const auth = sessionStorage.getItem("token");
    let url = "http://52.186.143.229/items/";
    const VehicleJson = JSON.stringify(vehicle);
    console.log(VehicleJson);
    sendAjaxPut(url, VehicleJson, successCallback, failureCallback, auth);
}


 
